/*
Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef _MPQ_STANDBY_UTILS_H
#define _MPQ_STANDBY_UTILS_H

#include <pthread.h>
#include <cutils/log.h>
#include <linux/mpq_mcu_gpio_comm.h>

#include <mpq_standby_hostcomm.h>

#define __FD_SET(fd, fdsetp) \
      (((fd_set *)(fdsetp))->fds_bits[(fd) >> 5] |= (1<<((fd) & 31)))
#define __FD_ZERO(fdsetp)   \
      (memset (fdsetp, 0, sizeof (*(fd_set *)(fdsetp))))

#define __FD_CLR(fd, fdsetp)   \
   (((fd_set *)(fdsetp))->fds_bits[(fd) >> 5] &= ~(1<<((fd) & 31)))

#ifdef LOG_TAG
#undef LOG_TAG
#endif

#define LOG_TAG "SP"

#define STANDBY_UART_DEV "/dev/ttyHSL2"
#define STANDBY_RC_DEV   "/dev/user-sp-input-dev0"
#define STANDBY_GPIO_DEV "/dev/mpq_mcu_gpio_dev0"

#define FALSE (0)
#define TRUE  (!FALSE)

#define PIPE_READ_FD  0
#define PIPE_WRITE_FD 1

#define MPQ_SP_DBG_MSG ALOGE

#define LE2BE_LONG(x)  ((x >> 24) & 0x000000FF) | \
                       ((x << 8)  & 0x00FF0000) | \
                       ((x >> 8)  & 0x0000FF00) | \
                       ((x << 24) & 0xFF000000)

#define LE2BE_SHORT(x) (x >> 8) | (x << 8)

typedef struct
{
   int        uart_fd;
   int        pipe_fd[2];
   pthread_t  uart_rx_tid;
   pthread_t  cmd_tid;
   int        mcu_gpio_fd;
} MPQ_STANDBY_HOST_CONTEXT;

typedef struct
{
   unsigned char size;
   unsigned char *buffer;
} MPQ_STANDBY_HOST_MSG;

typedef enum
{
   MPQ_GPIO_DIR_IN  = 0,
   MPQ_GPIO_DIR_OUT = 1
} MPQ_GPIO_DIR;

typedef enum
{
   MPQ_GPIO_LOW  = 0,
   MPQ_GPIO_HIGH = 1
} MPQ_GPIO_VALUE;

#define SET_MCU_BOOT_DLOAD  0x1
#define SET_MCU_BOOT_NORMAL 0x2
#define SET_MCU_WAKEUP_SRC  0x3

int mpq_standby_hostcomm_init(void);
int mpq_standby_hostcomm_term(void);

int mpq_standby_ir_rc_init(void);
int mpq_standby_ir_rc_term(void);
int mpq_standby_ir_rc_handler(unsigned char *buffer);

int mpq_standby_hdmi_cec_handler(unsigned char *buffer);
int mpq_standby_hotplug_handler(unsigned char *buffer);

int mpq_standby_upgrade_fw(void);
int mpq_standby_erase_flash(void);

int mpq_standby_mcu_gpio_ctrl(unsigned int cmd);


#endif /* _MPQ_STANDBY_UTILS_H */
