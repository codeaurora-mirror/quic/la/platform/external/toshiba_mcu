ifeq ($(TARGET_ARCH),arm)

LOCAL_PATH := $(call my-dir)
ifeq ($(call is-board-platform,msm8960),true)
include $(CLEAR_VARS)

LOCAL_CFLAGS := -D_ANDROID_
LOCAL_MODULE_TAGS := debug
LOCAL_MODULE := mpq_standby_app

LOCAL_SRC_FILES += mpq_standby_comm.c \
                   mpq_standby_cec.c \
                   mpq_standby_ir.c \
                   mpq_standby_hotplug.c \
                   mpq_standby_dload.c \
                   mpq_standby_main.c

LOCAL_ADDITIONAL_DEPENDENCIES := $(TARGET_OUT_INTERMEDIATES)/KERNEL_OBJ/usr

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../common
LOCAL_C_INCLUDES += $(TARGET_OUT_INTERMEDIATES)/KERNEL_OBJ/usr/include

LOCAL_SHARED_LIBRARIES := \
        libc \
        libcutils \
        libutils

include $(BUILD_EXECUTABLE)
endif
endif