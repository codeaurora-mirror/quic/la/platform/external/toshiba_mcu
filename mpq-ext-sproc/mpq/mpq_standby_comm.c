/*
Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <poll.h>
#include <assert.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include <mpq_standby_utils.h>

/* Acknowledgement Packet */
#define MPQ_SP_ACK_SIZE 3
const unsigned char mpq_sp_ack_packet[MPQ_SP_ACK_SIZE] =
                          {SP_COMM_STARTCODE, 1, SP_COMM_GRP_ACK};

#define MPQ_READY_SIZE 4
const unsigned char mpq_sp_ready_packet[MPQ_READY_SIZE] =
             {SP_COMM_STARTCODE, 2, SP_GRP_CTRL_MSG, SP_CTRL_MSG_MPQ_READY};

#define MPQ_NW_STANDBY_PKT_SIZE 4
const unsigned char mpq_nw_standby_packet[MPQ_NW_STANDBY_PKT_SIZE] =
             {SP_COMM_STARTCODE, 2, SP_GRP_CTRL_MSG,
                                    SP_CTRL_MPQ_STATE_NW_STANDBY};

#define MPQ_STANDBY_PKT_SIZE 4
const unsigned char mpq_standby_packet[MPQ_STANDBY_PKT_SIZE] =
             {SP_COMM_STARTCODE, 2, SP_GRP_CTRL_MSG,
                                    SP_CTRL_MPQ_STATE_STANDBY};


static MPQ_STANDBY_HOST_CONTEXT host_cntxt;
static unsigned char rx_buffer[255];

int mpq_standby_switch_state(int state)
{
   size_t bytes_ret = 0;
   int ret = 0;
   size_t size = 0;
   unsigned char *msg_ptr = NULL;

   switch (state)
   {
      case SP_CTRL_MPQ_STATE_NW_STANDBY:
         msg_ptr = mpq_nw_standby_packet;
         size    = MPQ_NW_STANDBY_PKT_SIZE;
         break;
      case SP_CTRL_MPQ_STATE_STANDBY:
         msg_ptr = mpq_standby_packet;
         size    = MPQ_STANDBY_PKT_SIZE;
         break;
      default:
         return -EINVAL;
   }

   bytes_ret = write(host_cntxt.uart_fd, msg_ptr, size);
   if (size != bytes_ret)
   {
       MPQ_SP_DBG_MSG("write() returned %d instead of %d at %u\n",
                      bytes_ret, size, __LINE__);
       ret = -EIO;
   }

   return ret;
}

static void *mpq_standby_uart_rx_handler(void *arg)
{
   int bytes_ret = 0;
   int loop_here  = TRUE;
   int i = 0;
   int ret = 0;
   unsigned char size = 0;
   unsigned char *buffer = NULL;
   MPQ_STANDBY_HOST_MSG msg;
   fd_set infids;

   MPQ_SP_DBG_MSG("In mpq_standby_uart_rx_handler \n");
   FD_ZERO (&infids);
   FD_SET (host_cntxt.uart_fd, &infids);

   while (loop_here)
   {
      /* Wait for data in Rx Buffer */
      if (select(host_cntxt.uart_fd + 1, &infids, NULL, NULL, NULL) < 1)
      {
         MPQ_SP_DBG_MSG("Select() Failed\n");
      }

      bytes_ret =  read(host_cntxt.uart_fd, rx_buffer, 255);

      //for (i = 0; i < bytes_ret; i++)
      //   MPQ_SP_DBG_MSG("0x%02x ", rx_buffer[i]);

      /* Search for Start Code */
      for (i = 0; i < bytes_ret; i++)
         if (SP_COMM_STARTCODE == rx_buffer[i])
            break;

      /* Start code found in entire received RX buffer */
      if (i == bytes_ret) continue;

      /* Start code  found. Set Size */
      size = rx_buffer[i + 1];
      //MPQ_SP_DBG_MSG("Received Message of Size : %d\n", size);

      /* Allocate the memory for packet */
      buffer = (unsigned char *)malloc(size * sizeof(unsigned char));
      if (NULL == buffer)
      {
         MPQ_SP_DBG_MSG("Failed to Allocate the Packet Buffer\n");
      }

     /* Copy Data to message buffer */
      memcpy(buffer, &rx_buffer[i + 2], size);

      /* Post the Packet to Command Handler Thread */
      msg.buffer = buffer;
      msg.size   = size;
      bytes_ret = write(host_cntxt.pipe_fd[PIPE_WRITE_FD], &msg,
                       sizeof(MPQ_STANDBY_HOST_MSG));
      if (bytes_ret != sizeof(MPQ_STANDBY_HOST_MSG))
      {
         MPQ_SP_DBG_MSG("write() returned %d instead of %d at %u\n",
                         bytes_ret, size, __LINE__);
      }
   }

   MPQ_SP_DBG_MSG("Exit mpq_standby_uart_rx_handler \n");

   return NULL;
}

int mpq_standby_ctrl_handler(unsigned char *buffer)
{
   int ret = 0;
   MPQ_SP_PROC_CAPS *sp_caps = NULL;

   if (NULL == buffer)
      return -EINVAL;

   switch (buffer[0])
   {
      case SP_CTRL_MSG_SP_CAPS:
         sp_caps = (MPQ_SP_PROC_CAPS *)&buffer[1];
         MPQ_SP_DBG_MSG("\n------ SP Firmware Details -----\n");
         MPQ_SP_DBG_MSG("Firmware Version : %02x\n", sp_caps->fw_version);
         MPQ_SP_DBG_MSG("IR Enable        : %02x\n", sp_caps->ir_enabled);
         MPQ_SP_DBG_MSG("CEC Enabled      : %02x\n", sp_caps->cec_enabled);
         break;
      default:
         break;
   }

   return ret;
}

static void *mpq_standby_command_handler(void *arg)
{
   int loop_here = TRUE;
   int bytes_ret = 0;
   MPQ_STANDBY_HOST_MSG msg;
   int ret = 0;
   int i = 0;

   MPQ_SP_DBG_MSG("In mpq_standby_command_handler\n");

   do
   {
      /* Get the message from pipe */
      bytes_ret = read(host_cntxt.pipe_fd[PIPE_READ_FD], &msg,
                       sizeof(MPQ_STANDBY_HOST_MSG));
      if (bytes_ret != sizeof(MPQ_STANDBY_HOST_MSG))
      {
         MPQ_SP_DBG_MSG("read() returned %d instead of %d at %u\n",
                         bytes_ret, sizeof(MPQ_STANDBY_HOST_MSG), __LINE__);
      }

      //MPQ_SP_DBG_MSG("Received the Message in Command Handler\n");

      /* Parse and Handle Messages Here */
      switch (msg.buffer[0])
      {
         case SP_COMM_GRP_ACK:
            break;
         case SP_GRP_IR_RC:
            ret = mpq_standby_ir_rc_handler(&msg.buffer[1]);
            if (ret)
               MPQ_SP_DBG_MSG("Failed in mpq_standby_ir_rc_handler : %d\n",
                              ret);
            break;
         case SP_GRP_CTRL_MSG:
            ret = mpq_standby_ctrl_handler(&msg.buffer[1]);
            if (ret)
               MPQ_SP_DBG_MSG("Failed in mpq_standby_ctrl_handler : %d\n",
                              ret);
            break;
         case SP_GRP_HDMI_CEC:
            ret = mpq_standby_hdmi_cec_handler(&msg.buffer[1]);
            if (ret)
               MPQ_SP_DBG_MSG("Failed in mpq_standby_hdmi_cec_handler %d\n",
                              ret);
            break;
         case SP_GRP_HOTPLUG_DETECT:
            ret = mpq_standby_hotplug_handler(&msg.buffer[1]);
            if (ret)
               MPQ_SP_DBG_MSG("Failed in mpq_standby_hotplug_handler %d\n",
                              ret);
            break;
      }

      /* Free the message buffer */
      free(msg.buffer);

   } while(loop_here);

   MPQ_SP_DBG_MSG("Exit mpq_standby_command_handler\n");

   return NULL;
}

static int mpq_standby_uart_init(void)
{
   int ret = 0;
   struct termios options;

   host_cntxt.uart_fd = open(STANDBY_UART_DEV, O_RDWR | O_NOCTTY | O_NDELAY);
   if (host_cntxt.uart_fd < 0)
   {
      MPQ_SP_DBG_MSG("Failed to open UART device : %d\n", host_cntxt.uart_fd);
      return -ENODEV;
   }

   ret = tcflush(host_cntxt.uart_fd, TCIOFLUSH);
   if (ret)
   {
      MPQ_SP_DBG_MSG("Failed in tcflush with %d\n", ret);
      goto cleanup;
   }

   ret = tcgetattr(host_cntxt.uart_fd, &options);
   if (ret)
   {
      MPQ_SP_DBG_MSG("Failed in tcgetattr with %d\n", ret);
      goto cleanup;
   }

   options.c_cc[VTIME]  = 0; /* inter-character timer unused */
   options.c_cc[VMIN]   = 1; /* blocking read until 5 chars received */
   options.c_cflag     &= ~CSIZE;
   options.c_cflag     &= ~CRTSCTS;
   options.c_cflag     &= ~CSTOPB;
   options.c_cflag     |= (CS8 | CLOCAL | CREAD);
   options.c_iflag      = IGNPAR;
   options.c_oflag      = 0;
   options.c_lflag      = 0;
   cfsetospeed(&options, B115200);
   cfsetispeed(&options, B115200);

   ret = tcsetattr(host_cntxt.uart_fd, TCSANOW, &options);
   if (ret)
   {
      MPQ_SP_DBG_MSG("Failed in tcsetattr with %d\n", ret);
      goto cleanup;
   }

   fcntl(host_cntxt.uart_fd, F_SETFL, 0);

   MPQ_SP_DBG_MSG("UART Initialization complete\n");

   return 0;

cleanup:
   close(host_cntxt.uart_fd);
   return ret;
}

int mpq_standby_hostcomm_term(void)
{
   if (host_cntxt.cmd_tid)
      pthread_join(host_cntxt.cmd_tid, NULL);

   if (host_cntxt.uart_rx_tid)
      pthread_join(host_cntxt.uart_rx_tid, NULL);

   if (host_cntxt.pipe_fd[PIPE_READ_FD])
   {
      close(host_cntxt.pipe_fd[PIPE_READ_FD]);
      close(host_cntxt.pipe_fd[PIPE_WRITE_FD]);
   }

   if (host_cntxt.uart_fd)
      close(host_cntxt.uart_fd);

   return 0;
}

int mpq_standby_mcu_gpio_ctrl(unsigned int cmd)
{
   int ret = 0;

   switch (cmd)
   {
      case SET_MCU_BOOT_DLOAD:
         ret = ioctl(host_cntxt.mcu_gpio_fd, MPQ_SET_MCU_BOOT_DLOAD);
         break;
      case SET_MCU_BOOT_NORMAL:
         ret = ioctl(host_cntxt.mcu_gpio_fd, MPQ_SET_MCU_BOOT_NORMAL);
         break;
      case SET_MCU_WAKEUP_SRC:
         ret = ioctl(host_cntxt.mcu_gpio_fd, MPQ_SET_MCU_WAKEUP_SRC);
         break;
   }

   return ret;
}

int mpq_standby_mcu_gpio_init(void)
{
   int ret = 0;
   host_cntxt.mcu_gpio_fd = open(STANDBY_GPIO_DEV, O_RDWR);

   if (host_cntxt.mcu_gpio_fd < 0)
   {
      MPQ_SP_DBG_MSG("%s open failed with %d\n", STANDBY_GPIO_DEV,
                     host_cntxt.mcu_gpio_fd);
      return -ENODEV;
   }

   //MPQ_SP_DBG_MSG("MCU GPIO FD %d\n", host_cntxt.mcu_gpio_fd);

   return 0;
}

static int mpq_standby_check_mcu_caps(void)
{
   int bytes_ret = 0;
   int dload = FALSE;
   int erase = FALSE;
   fd_set infids;
   struct timeval tv;
   MPQ_SP_PROC_CAPS *pMCUCaps = NULL;
   int ret = 0;
   int x = 0;

   if (erase)
   {
      MPQ_SP_DBG_MSG("Switch MCU to Erase Mode\n");

      /* Close UART */
      close(host_cntxt.uart_fd);

      /* Erase Flash */
      ret = mpq_standby_erase_flash();
      if (ret)
      {
         MPQ_SP_DBG_MSG("Failed in MCU Flash Erase with %d\n", ret);
      }

      goto cleanup;
   }

   /* Send MPQ_READY Message */
   bytes_ret = write(host_cntxt.uart_fd, mpq_sp_ready_packet, MPQ_READY_SIZE);
   if (MPQ_READY_SIZE != bytes_ret)
   {
       MPQ_SP_DBG_MSG("write() returned %d instead of %d at %u\n",
                      bytes_ret, MPQ_READY_SIZE, __LINE__);
   }

   /* Wait for SP Capabilities Message */
   FD_ZERO (&infids);
   FD_SET (host_cntxt.uart_fd, &infids);
   tv.tv_sec  = 1;
   tv.tv_usec = 0;
   ret = select(host_cntxt.uart_fd + 1, &infids, NULL, NULL, &tv);
   if (ret < 0)
   {
      MPQ_SP_DBG_MSG("Failed in select() : %d\n", ret);
      goto cleanup;
   }
   else if (ret == 0)
   {
      MPQ_SP_DBG_MSG("No SP CAPS message received from MCU\n");
      MPQ_SP_DBG_MSG("Initiating MCU DLOAD\n");
      dload = TRUE;
   }
   else
   {
      bytes_ret =  read(host_cntxt.uart_fd, rx_buffer,
                        sizeof(MPQ_SP_PROC_CAPS) + 4);

      /* Check SP Caps Packet Syntax */
      if ((bytes_ret == sizeof(MPQ_SP_PROC_CAPS) + 4) &&
          (rx_buffer[HOSTCOMM_STARTCODE_INDEX] == SP_COMM_STARTCODE) &&
          (rx_buffer[HOSTCOMM_GROUP_INDEX] == SP_GRP_CTRL_MSG) &&
          (rx_buffer[HOSTCOMM_COMMAND_INDEX] == SP_CTRL_MSG_SP_CAPS))
      {
         pMCUCaps = (MPQ_SP_PROC_CAPS *)&rx_buffer[HOSTCOMM_DATA_INDEX];

         MPQ_SP_DBG_MSG("\n------ SP Firmware Details -----\n");
         MPQ_SP_DBG_MSG("Firmware Version : %02x\n", pMCUCaps->fw_version);
         MPQ_SP_DBG_MSG("IR Enable        : %02x\n", pMCUCaps->ir_enabled);
         MPQ_SP_DBG_MSG("CEC Enabled      : %02x\n", pMCUCaps->cec_enabled);

         if (pMCUCaps->fw_version != SP_FIRMWARE_VERSION)
         {
            MPQ_SP_DBG_MSG("Firmware Upgrade Required \n");
            dload = TRUE;
         }
         ret = 0;
      }
      else
      {
         for (x = 0; x < bytes_ret; x++)
            MPQ_SP_DBG_MSG("0x%02x ", rx_buffer[x]);

         MPQ_SP_DBG_MSG("Invalid Message received instead of SP Caps\n");
         dload = TRUE;
      }
   }

   /* Failed to get a messge - Switch to DLOAD mode */
   if (dload)
   {
      MPQ_SP_DBG_MSG("Switch MCU to DLOAD Mode\n");

      /* Close UART */
      close(host_cntxt.uart_fd);

      /* Upgrade MCU Firmware */
      ret = mpq_standby_upgrade_fw();
      if (ret)
      {
         MPQ_SP_DBG_MSG("Failed in MCU F/W Upgrade with %d\n", ret);
         goto cleanup;
      }
   }

cleanup:
   return ret;
}

int mpq_standby_hostcomm_init(void)
{
   int ret = 0;

   /* Clear the fields */
   memset((void *)&host_cntxt, 0x00, sizeof(MPQ_STANDBY_HOST_CONTEXT));

   /* Initialize the GPIO required */
   ret = mpq_standby_mcu_gpio_init();
   if (ret)
   {
      MPQ_SP_DBG_MSG("Failed in mpq_standby_gpio_init with %d\n", ret);
      goto cleanup;
   }

   /* Initialize the UART */
   ret = mpq_standby_uart_init();
   if (ret)
   {
      MPQ_SP_DBG_MSG("Failed in mpq_standby_uart_init with %d\n", ret);
      goto cleanup;
   }

   /* Check MCU Capabilities */
   ret = mpq_standby_check_mcu_caps();
   if (ret)
   {
      MPQ_SP_DBG_MSG("Failed in mpq_standby_check_mcu_caps with %d\n", ret);
      goto cleanup;
   }

   /* Create pipe for communication between threads */
   ret = pipe(host_cntxt.pipe_fd);
   if (ret)
   {
      MPQ_SP_DBG_MSG("Failed to create pipe with %d\n", ret);
      goto cleanup;
   }

   /* Create the UART RX Thread */
   ret = pthread_create(&host_cntxt.uart_rx_tid, 0,
                        mpq_standby_uart_rx_handler, NULL);
   if (ret)
   {
      MPQ_SP_DBG_MSG("Failed in UART Rx pthread_create with %d\n", ret);
      goto cleanup;
   }

   /* Create the Command Handler Thread */
   ret = pthread_create(&host_cntxt.cmd_tid, 0,
                        mpq_standby_command_handler, NULL);
   if (ret)
   {
      MPQ_SP_DBG_MSG("Failed in Command pthread_create with %d\n", ret);
      goto cleanup;
   }

   return 0;

cleanup:
   mpq_standby_hostcomm_term();
   return ret;
}

