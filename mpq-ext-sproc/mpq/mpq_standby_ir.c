/*
Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/poll.h>
#include <assert.h>
#include <pthread.h>
#include <linux/input.h>

#include <mpq_standby_utils.h>

#define MPQ_RC_KEY_PRESSED    0x01
#define MPQ_RC_KEY_REPEATED   0x02
#define MPQ_RC_KEY_RELEASED   0x03

#define MPQ_RC_MASK_RC6  0xFF
#define MPQ_RC_MASK_NEC  0xFFFFFF
#define MPQ_IR_MESG_SIZE 5

static int ir_rc_fd = 0;

int mpq_standby_ir_rc_init(void)
{
   int ret = 0;

   ir_rc_fd = open(STANDBY_RC_DEV, O_RDWR);
   if (ir_rc_fd < 0)
   {
      ret = -ENODEV;
      MPQ_SP_DBG_MSG("Failed to Open the %s Device", STANDBY_RC_DEV);
   }

   return ret;
}

static int mpq_standby_ir_rc_key_event(int scancode)
{
   int bytes_ret = 0;
   int ret = 0;
   unsigned char buffer[MPQ_IR_MESG_SIZE] = {0};

   buffer[0] = MPQ_RC_KEY_PRESSED;
   *(int *)&buffer[1] = (scancode & MPQ_RC_MASK_NEC);

   MPQ_SP_DBG_MSG("<%d> Scan Code : 0x%08x\n", ir_rc_fd, scancode);

   bytes_ret = write(ir_rc_fd, buffer, MPQ_IR_MESG_SIZE);
   if (MPQ_IR_MESG_SIZE != bytes_ret)
   {
       MPQ_SP_DBG_MSG("Wrote %d bytes instaed of %d bytes",
                      bytes_ret, MPQ_IR_MESG_SIZE);
       ret = -EINVAL;
   }

   return ret;
}

int mpq_standby_ir_rc_handler(unsigned char *buffer)
{
   int ret = 0;

   if (NULL == buffer)
      return -EINVAL;

   switch (buffer[0])
   {
      case SP_IR_SCANCODE_RCVD_EVENT:
         ret = mpq_standby_ir_rc_key_event(*(int *)&buffer[1]);
         break;
      default:
         break;
   }

   return ret;
}

int mpq_standby_ir_rc_term(void)
{
   int ret = 0;

   if (ir_rc_fd)
     ret = close(ir_rc_fd);

   return ret;
}

