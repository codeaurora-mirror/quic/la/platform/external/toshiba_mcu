/*
 * Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
 * Not a Contribution, Apache license notifications and license are retained
 * for attribution purposes only.
 *
 * Copyright (c) 2010 TOSHIBA CORPORATION, All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _SP_CEC_H
#define _SP_CEC_H

#include "tmpm330_cec.h"

typedef enum {
   DATA_INIT = 0,
   DATA_IN_PROGRESS = 1,
   DATA_END = 2
} CEC_DataState;

typedef struct {
   CEC_LogicalAddr Initiator;
   CEC_LogicalAddr Destination;
   unsigned char Opcode;
   unsigned char CEC_Data[17];
   unsigned char current_num;
   unsigned char Max_num;
   CEC_DataState current_state;
} CEC_FrameTypeDef;

void SP_CECInit(void);
void SP_HandleCECInput(void);

#endif /* _SP_CEC_H */
