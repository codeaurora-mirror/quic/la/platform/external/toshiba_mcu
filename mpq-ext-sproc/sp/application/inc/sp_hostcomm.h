/*
 * Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
 * Not a Contribution, Apache license notifications and license are retained
 * for attribution purposes only.
 *
 * Copyright (c) 2010 TOSHIBA CORPORATION, All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _SP_HOSTCOMM_H
#define _SP_HOSTCOMM_H

#include "sp_common.h"
#include "mpq_standby_hostcomm.h"

#define UART_RING_BUFFER_SIZE 100
typedef struct _uart_data_ring_buffer
{
   unsigned char filled_size;
   unsigned char ring_buffer[UART_RING_BUFFER_SIZE];
} SP_UART_RING_BUFFER;

typedef enum _host_comm_state
{
   HOST_COMM_SCODE_SEARCH,
   HOST_COMM_SIZE_SEARCH,
   HOST_COMM_RX_DATA,
   HOST_COMM_PROCESSING
} SP_HOST_COMM_STATE;

typedef enum _mpq_state
{
   MPQ_STATE_ACTIVE,
   MPQ_STATE_SUSPEND,
   MPQ_STATE_POWERDOWN
} SP_MPQ_STATE;

void SP_HostCommInit(void);
void SP_HostCommHandlePacket(void);
void SP_HostUARTSendData(unsigned char size);
void SP_HostCommSendSPCaps(void);
void SP_WakeupMPQ(void);

#endif /* _SP_HOSTCOMM_H */
