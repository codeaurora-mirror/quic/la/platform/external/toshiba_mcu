/*
 * Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
 * Not a Contribution, Apache license notifications and license are retained
 * for attribution purposes only.
 *
 * Copyright (c) 2010 TOSHIBA CORPORATION, All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <string.h>

#include "sp_common.h"
#include "sp_hostcomm.h"
#include "tmpm330_uart.h"
#include "tmpm330_gpio.h"
#include "sp_ir.h"

extern volatile unsigned char sleep;
extern volatile SP_MPQ_STATE mpq_state;
extern volatile unsigned short count;

SP_UART_RING_BUFFER hostcomm_rb;
SP_HOST_COMM_STATE  hostcomm_state;

unsigned char tx_buffer[UART_RING_BUFFER_SIZE];
unsigned char tx_counter;
volatile unsigned char tx_complete = FALSE;

void INTTX0_IRQHandler(void)
{
   volatile UART_Err err;
   unsigned char tx_buffer_size = tx_buffer[HOSTCOMM_SIZE_INDEX] + 2;

   if (tx_counter < tx_buffer_size)
   {
      UART_SetTxData(UART0, tx_buffer[tx_counter++]);
   }
   else
   {
      err = UART_GetErrState(UART0);
      while(UART0->MOD2 & 0x20);
      tx_complete = TRUE;
      sleep = TRUE;
   }
}

void INTRX0_IRQHandler(void)
{
   static unsigned char size = 0;
   volatile UART_Err err;
   unsigned char data;

   data = (uint8_t) UART_GetRxData(UART0);
   err = UART_GetErrState(UART0);
   if (UART_NO_ERR == err)
   {
      switch (hostcomm_state)
      {
         case HOST_COMM_SCODE_SEARCH:
            if (SP_COMM_STARTCODE == data)
            {
               hostcomm_rb.filled_size = size = 0;
               hostcomm_state = HOST_COMM_SIZE_SEARCH;
            }
            break;
         case HOST_COMM_SIZE_SEARCH:
            hostcomm_state = HOST_COMM_RX_DATA;
            size = data;
            break;
         case HOST_COMM_RX_DATA:
            hostcomm_rb.ring_buffer[hostcomm_rb.filled_size++] =
                                (unsigned char) data;
            if (hostcomm_rb.filled_size == size)
               hostcomm_state = HOST_COMM_PROCESSING;
            break;
         case HOST_COMM_PROCESSING:
            /* Error to get an interrupt when in processing state */
            break;
      }
   }
}

static void SP_HostUARTInit(void)
{
   UART_InitTypeDef myUART;

   SIO_Configuration(UART0);

   myUART.BaudRate = 115200U;
   myUART.DataBits = UART_DATA_BITS_8;
   myUART.StopBits = UART_STOP_BITS_1;
   myUART.Parity = UART_NO_PARITY;
   myUART.Mode = UART_ENABLE_RX | UART_ENABLE_TX;
   myUART.FlowCtrl = UART_NONE_FLOW_CTRL;

   UART_Enable(UART0);
   UART_Init(UART0, &myUART);
   UART_SetIdleMode(UART0, ENABLE);

   NVIC_Enable_Interrupt(INTTX0_interrupt);
   NVIC_Enable_Interrupt(INTRX0_interrupt);
}

void SP_HostUARTSendData(unsigned char size)
{
   /* Fill in Start Code and Size */
   sleep = FALSE;

   tx_buffer[HOSTCOMM_STARTCODE_INDEX] = SP_COMM_STARTCODE;
   tx_buffer[HOSTCOMM_SIZE_INDEX] = size;

   /* Reset the Tx Counter */
   tx_counter = 0;
   tx_complete = FALSE;

   /* Send the Data through UART */
   UART_SetTxData(UART0, tx_buffer[tx_counter++]);
   while (tx_complete == FALSE);
}

void SP_WakeupMPQ(void)
{
   volatile int i = 0;
   TMRB_SetCaptureTiming(TSB_TB0, TMRB_DISABLE_CAPTURE);
   GPIO_SetInputEnableReg(GPIO_PH, GPIO_BIT_0, DISABLE);
   GPIO_SetOutputEnableReg(GPIO_PH, GPIO_BIT_0, ENABLE);
   GPIO_DisableFuncReg(GPIO_PH, GPIO_FUNC_REG_1, GPIO_BIT_0);

   GPIO_WriteDataBit(GPIO_PH, GPIO_BIT_0, GPIO_BIT_VALUE_0);
   for (i = 0; i < 10; i++);

   GPIO_SetOutputEnableReg(GPIO_PH, GPIO_BIT_0, DISABLE);
   GPIO_SetInputEnableReg(GPIO_PH, GPIO_BIT_0, ENABLE);
   GPIO_EnableFuncReg(GPIO_PH, GPIO_FUNC_REG_1, GPIO_BIT_0);
   TMRB_SetCaptureTiming(TSB_TB0, TMRB_CAPTURE_IN_RISING);
}

unsigned char IRFrameReceived = FALSE;
unsigned int  IR_Command;
unsigned int  toggle_bit = 0x1;

void SP_HandleIRKey(void)
{
   if (IRFrameReceived == TRUE)
   {
      #if(defined(RC5_FORMAT_RMC))
      if ((IR_Command & 0x800) == toggle_bit)
      {
         return;
      }
      #endif

      if ((IR_Command & RMC_POWER_KEY_MASK) == RMC_POWER_KEY)
      {
         SP_HandleMPQWakeup();
      }

      if (mpq_state == MPQ_STATE_POWERDOWN)
      {
         IRFrameReceived = FALSE;
         #if(defined(RC5_FORMAT_RMC))
         toggle_bit = (IR_Command & 0x800);
         #endif
         return;
      }

      /* Send IR Command to Host */
      tx_buffer[HOSTCOMM_GROUP_INDEX]   = SP_GRP_IR_RC;
      tx_buffer[HOSTCOMM_COMMAND_INDEX] = SP_IR_SCANCODE_RCVD_EVENT;

      memcpy(&tx_buffer[HOSTCOMM_DATA_INDEX], &IR_Command, sizeof(unsigned int));
      SP_HostUARTSendData(sizeof(unsigned int) + 2);

      /* Reset the flag */
      IRFrameReceived = FALSE;
      #if(defined(RC5_FORMAT_RMC))
      toggle_bit = (IR_Command & 0x800);
      #endif
   }
}

void SP_HostCommSendSPCaps(void)
{
   MPQ_SP_PROC_CAPS sp_caps = {0};

   tx_buffer[HOSTCOMM_GROUP_INDEX]   = SP_GRP_CTRL_MSG;
   tx_buffer[HOSTCOMM_COMMAND_INDEX] = SP_CTRL_MSG_SP_CAPS;

   sp_caps.fw_version  = SP_FIRMWARE_VERSION;
   sp_caps.ir_enabled  = TRUE;
   sp_caps.cec_enabled = TRUE;

   memcpy(&tx_buffer[HOSTCOMM_DATA_INDEX], &sp_caps, sizeof(MPQ_SP_PROC_CAPS));
   SP_HostUARTSendData(sizeof(MPQ_SP_PROC_CAPS) + 2);
}

void SP_HandleControlMsg(void)
{
   switch (hostcomm_rb.ring_buffer[1])
   {
       case SP_CTRL_MSG_MPQ_READY:
          if (mpq_state == MPQ_STATE_POWERDOWN)
          {
             SP_HostCommSendSPCaps();
             mpq_state = MPQ_STATE_ACTIVE;
             count = 0;
          }
          break;
       default:
          /* Do nothing */
         break;
   }
}

void SP_HostCommHandlePacket(void)
{
   if (HOST_COMM_PROCESSING == hostcomm_state)
   {
      /* Get the Size of the Data */

      /* Check the Group */

      /* Call the appropriate function */
      switch (hostcomm_rb.ring_buffer[0])
      {
         case SP_COMM_GRP_ACK:
            /* Received Acknowledgement */
            break;
         case SP_GRP_CTRL_MSG:
            SP_HandleControlMsg();
            break;
         default:
            break;
      }

      /* Release the buffer for next message */
      INT_LOCK();
      hostcomm_rb.filled_size = 0;
      hostcomm_state = HOST_COMM_SCODE_SEARCH;
      INT_FREE();
   }
}

void SP_HostCommInit(void)
{
   /* Initialize the Host Communication UART */
   SP_HostUARTInit();

   /* Reset the Read and Write Indexes */
   INT_LOCK();
   hostcomm_rb.filled_size = 0;
   hostcomm_state = HOST_COMM_SCODE_SEARCH;
   INT_FREE();
}
