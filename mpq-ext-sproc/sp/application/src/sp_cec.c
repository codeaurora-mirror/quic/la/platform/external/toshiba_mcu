/*
 * Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
 * Not a Contribution, Apache license notifications and license are retained
 * for attribution purposes only.
 *
 * Copyright (c) 2010 TOSHIBA CORPORATION, All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string.h>
#include "sp_common.h"
#include "sp_debugcomm.h"
#include "sp_hostcomm.h"
#include "tmpm330_cec.h"
#include "sp_cec.h"

CEC_FrameTypeDef CEC_RxFrame;
CEC_FrameTypeDef CEC_RxFrameTmp;

extern unsigned char tx_buffer[UART_RING_BUFFER_SIZE];

void INTCECRX_IRQHandler(void)
{
   CEC_DataTypeDef data;
   CEC_RxINTState stateRx;

   /* Clear CEC Interrupt */
   TSB_CG->ICRCG = 0x06U;
   data = CEC_GetRxData();
   stateRx = CEC_GetRxINTState();

   if (stateRx.Bit.RxEnd == 1)
   {
      if (CEC_RxFrameTmp.current_num < 17U)
      {
         CEC_RxFrameTmp.CEC_Data[CEC_RxFrameTmp.current_num] = data.Data;
         CEC_RxFrameTmp.current_num++;
         CEC_RxFrameTmp.current_state = DATA_IN_PROGRESS;
      }
      if (data.EOMBit == CEC_EOM)
      {
         CEC_RxFrameTmp.current_state = DATA_END;
      }
   }
   else if (stateRx.Bit.RxStartBit == 1)
   {
      /*Do nothing */
   }
   else
   {
      CEC_RxFrameTmp.current_state = DATA_END;
   }
}

void SP_CECInit(void)
{
   unsigned int i = 0;

   /* Initialize CEC port */
   GPIO_SetOutputEnableReg(GPIO_PK, GPIO_BIT_0, ENABLE);
   GPIO_EnableFuncReg(GPIO_PK, GPIO_FUNC_REG_1, GPIO_BIT_0);
   GPIO_SetInputEnableReg(GPIO_PK, GPIO_BIT_0, ENABLE);

   /* Initialize CEC frame data buffer */
   CEC_RxFrame.Initiator = CEC_UNKNOWN;
   CEC_RxFrame.Destination = CEC_UNKNOWN;
   CEC_RxFrame.Opcode = 0xFFU;
   CEC_RxFrame.current_num = 0x0U;
   CEC_RxFrame.Max_num = 0x0U;
   CEC_RxFrame.current_state = DATA_INIT;
   for (i = 0U; i < 17U; i++)
   {
      CEC_RxFrame.CEC_Data[i] = 0xFFU;
   }

   memcpy(&CEC_RxFrameTmp, &CEC_RxFrame, sizeof(CEC_FrameTypeDef));

   /* Initialize CEC register */
   CEC_Enable();
   CEC_SWReset();
   while (CEC_GetRxState() == ENABLE);
   while (CEC_GetTxState() == BUSY);
   CEC_DefaultConfig();

   /* Initialize CEC interupt */
   INT_LOCK();
   TSB_CG->IMCGB = 0x00300000U;
   TSB_CG->IMCGD = 0x00000030U;
   TSB_CG->IMCGB = 0x00310000U;
   TSB_CG->IMCGD = 0x00000031U;
   TSB_CG->ICRCG = 0x06U;
   TSB_CG->ICRCG = 0x0cU;

   CEC_SetIdleMode(ENABLE);

   NVIC_Enable_Interrupt(INTCECRX_interrupt);
   INT_FREE();

   CEC_SetLogicalAddr(CEC_TV);

   /* Enable CEC Reception */
   CEC_SetRxCtrl(ENABLE);
}

void SP_HandleCECInput(void)
{
   unsigned int i, temp;

   if (CEC_RxFrameTmp.current_state == DATA_END)
   {
      memcpy(&CEC_RxFrame, &CEC_RxFrameTmp, sizeof(CEC_FrameTypeDef));
      CEC_RxFrameTmp.current_num = 0U;
      CEC_RxFrameTmp.current_state = DATA_INIT;
      temp = CEC_RxFrame.current_num;

      if (temp > 0U)
      {
         /* Send CEC Command to Host */
         tx_buffer[HOSTCOMM_GROUP_INDEX]   = SP_GRP_HDMI_CEC;
         tx_buffer[HOSTCOMM_COMMAND_INDEX] = SP_HDMI_CEC_MSG_EVENT;

         memcpy(&tx_buffer[HOSTCOMM_DATA_INDEX], &CEC_RxFrame, sizeof(CEC_FrameTypeDef));
         SP_HostUARTSendData(sizeof(CEC_FrameTypeDef) + 2);
      }

      for (i = 0U; i < 17U; i++)
      CEC_RxFrameTmp.CEC_Data[i] = 0xFFU;
   }
}
