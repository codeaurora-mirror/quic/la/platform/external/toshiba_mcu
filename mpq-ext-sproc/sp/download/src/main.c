/*
 * Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
 * Not a Contribution, Apache license notifications and license are retained
 * for attribution purposes only.
 *
 * Copyright (c) 2010 TOSHIBA CORPORATION, All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "TMPM330.h"
#include "tx03_common.h"
#include "tmpm330_gpio.h"
#include "tmpm330_tmrb.h"
#include "tmpm330_cg.h"
#include "tmpm330_fc.h"
#include "tmpm330_uart.h"

#define TRMB_MILLI_SEC  (6000)
#define PAGE_SIZE (512U)
#define TERMINATION_STR "TERMINATE"

UART_InitTypeDef myUART;
uint8_t rec_data[PAGE_SIZE];

/* Macros for LED ON/OFF */
#define RED_ON GPIO_WriteDataBit(GPIO_PI, GPIO_BIT_0, GPIO_BIT_VALUE_1);
#define RED_OFF GPIO_WriteDataBit(GPIO_PI, GPIO_BIT_0, GPIO_BIT_VALUE_0);

#define GREEN_ON GPIO_WriteDataBit(GPIO_PI, GPIO_BIT_1, GPIO_BIT_VALUE_1);
#define GREEN_OFF GPIO_WriteDataBit(GPIO_PI, GPIO_BIT_1, GPIO_BIT_VALUE_0);

int compare_str(const char *s1, const char *s2,uint32_t strlenth)
{
   int i;

   for(i = 0; i < strlenth; i++)
   {
      if(s1[i] != s2[i])
         return 1;
   }
   return 0;
}

void SIO_Configuration(TSB_SC_TypeDef * SCx)
{
   if (SCx == UART0)
   {
      TSB_PE_CR_PE0C = 1U;
      TSB_PE_FR1_PE0F1 = 1U;
      TSB_PE_FR1_PE1F1 = 1U;
      TSB_PE_IE_PE1IE = 1U;
   }
   else if (SCx == UART1)
   {
      TSB_PE_CR_PE4C = 1U;
      TSB_PE_FR1_PE4F1 = 1U;
      TSB_PE_FR1_PE5F1 = 1U;
      TSB_PE_IE_PE5IE = 1U;
   }
   else if (SCx == UART2)
   {
      TSB_PF_CR_PF0C = 1U;
      TSB_PF_FR1_PF0F1 = 1U;
      TSB_PF_FR1_PF1F1 = 1U;
      TSB_PF_IE_PF1IE = 1U;
   }
}

void SP_WaitForDelay(unsigned int delay_ms)
{
   TMRB_InitTypeDef myTMRB;
   TMRB_FFOutputTypeDef ffopTMRB;

   myTMRB.Mode = TMRB_INTERVAL_TIMER;
   myTMRB.ClkDiv = TMRB_CLK_DIV_8;
   myTMRB.Cycle = TRMB_MILLI_SEC;
   myTMRB.UpCntCtrl = TMRB_FREE_RUN;
   myTMRB.Duty = TRMB_MILLI_SEC / 2U;

   TMRB_Enable(TSB_TB0);
   TMRB_SetRunState(TSB_TB0, TMRB_STOP);
   TMRB_SetDoubleBuf(TSB_TB0, DISABLE);
   TMRB_Init(TSB_TB0, &myTMRB);

   ffopTMRB.FlipflopCtrl = TMRB_FLIPFLOP_CLEAR;
   ffopTMRB.FlipflopReverseTrg = (TMRB_FLIPFLOP_MATCH_CYCLE | TMRB_FLIPFLOP_MATCH_DUTY);
   TMRB_SetFlipFlop(TSB_TB0, &ffopTMRB);
   TMRB_SetCaptureTiming(TSB_TB0, TMRB_DISABLE_CAPTURE);

   TMRB_SetRunState(TSB_TB0, TMRB_RUN);

   /* Wait for the timer to stop */
   while ((TSB_TB0->ST & 0x2) == 0);

   TMRB_SetRunState(TSB_TB0, TMRB_STOP);
   TMRB_Disable(TSB_TB0);
}

unsigned char Write_Flash(unsigned int * addr_dest, unsigned int * addr_source, unsigned int len)
{
   unsigned int size;
   unsigned int *source;
   unsigned int *dest;

   dest = addr_dest;
   source = addr_source;
   size = len;

   while (size > PAGE_SIZE)
   {
      if (FC_SUCCESS == FC_WritePage((unsigned int) dest, source))
      {
         /* Do nothing */
      }
      else
      {
         return ERROR;
      }
      size = size - PAGE_SIZE;
      dest = dest + PAGE_SIZE / 4U;
      source = source + PAGE_SIZE / 4U;
   }

   RED_ON
   GREEN_ON

   if (FC_SUCCESS == FC_WritePage((unsigned int) dest, source))
   {
      /* Do nothing */
   }
   else
   {
      RED_ON
      GREEN_OFF
      return ERROR;
   }

   RED_OFF 
   GREEN_ON

   return SUCCESS;
}

unsigned char uart_copy(void)
{
   int ret_val = 0;
   unsigned int i, flash_add = 0;

   SIO_Configuration(UART0);
   myUART.BaudRate = 115200U;
   myUART.DataBits = UART_DATA_BITS_8;
   myUART.StopBits = UART_STOP_BITS_1;
   myUART.Parity = UART_NO_PARITY;
   myUART.Mode = UART_ENABLE_RX | UART_ENABLE_TX;
   myUART.FlowCtrl = UART_NONE_FLOW_CTRL;

   UART_Enable(UART0);
   UART_Init(UART0, &myUART);

   flash_add = 0x3F800000;
   while (1)
   {
      ret_val = UART_GetBufState(UART0, UART_TX);
      if (ret_val == DONE)
      {
         UART_SetTxData(UART0, 'S');
      }

      for(i = 0;i < PAGE_SIZE;)
      {
         ret_val = UART_GetBufState(UART0, UART_RX);
         if(ret_val == DONE)
         {
            rec_data[i] = UART_GetRxData(UART0);
            i++;
         }
      }

      /* Whole page is not received yet */
      if (i < PAGE_SIZE)
         continue;

      /* Check terminate command */
      if (0 == compare_str((const char *)rec_data, TERMINATION_STR, sizeof(TERMINATION_STR) - 1))
         break;

      RED_ON
      GREEN_OFF

      if (FC_SUCCESS == Write_Flash((unsigned int *)flash_add, (unsigned int *)rec_data, PAGE_SIZE))
      {
         for(i = 0; i < PAGE_SIZE; i++)
           rec_data[i] = 0;
      }
      else
      {
         return ERROR;
      }

      flash_add = flash_add + PAGE_SIZE;
   }

    return 0;
}

int main(void)
{
   SystemInit();

   __disable_irq();

   /* Pin Configuration for LED - PI0, PI1 */
   GPIO_SetOutputEnableReg(GPIO_PI, GPIO_BIT_0, ENABLE);
   GPIO_WriteDataBit(GPIO_PI, GPIO_BIT_0, GPIO_BIT_VALUE_0);

   GPIO_SetOutputEnableReg(GPIO_PI, GPIO_BIT_1, ENABLE);
   GPIO_WriteDataBit(GPIO_PI, GPIO_BIT_1, GPIO_BIT_VALUE_0);

   uart_copy();

   return 0;
}

