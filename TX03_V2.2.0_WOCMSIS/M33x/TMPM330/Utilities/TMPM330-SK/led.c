/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* File Name          : led.c
* Version            : 
* Date               : 
* Description        : 
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "led.h"

/*******************************************************************************
* Function Name  : LED_IO
* Description    : Config the GPIO to LED 
* Input          : None 
* Return         : None.
*******************************************************************************/
void LED_IO(void)
{
    TSB_PG->CR |= 0x0F;
    TSB_PG->PUP |= 0x0F;
    TSB_PJ->CR |= 0x0F;
    TSB_PJ->PUP |= 0x0F;
}

/*******************************************************************************
* Function Name  : LED_Display
* Description    : Turn on or off LED
* Input          : ucLED
* Return         : None.
*******************************************************************************/
void LED_Display(uint8_t ucLED)
{
    uint8_t tmp = 0U;
    tmp = ucLED & 0x0f;
    TSB_PG->DATA = tmp;
    tmp = ((uint8_t) (ucLED >> 4) & 0x0f);
    TSB_PJ->DATA = tmp;
}


/*******************************************************************************
* Function Name  : LED_Configuration
* Description    : Configures LED
* Input          : None.
* Return         : None.
*******************************************************************************/
void LED_Configuration(void)
{
    LED_IO();

    /* LED initialization state */
    LED_Display(LEDALL_OFF);
}

/*******************************************************************************
* Function Name  : LED_On
* Description    : Turn on LED
* Input          : ucLED
* Return         : None.
*******************************************************************************/
void LED_On(uint8_t ucLED)
{
    if (ucLED & LED1) {
        TSB_PG_DATA_PG0 = 1;
    }
    if (ucLED & LED2) {
        TSB_PG_DATA_PG1 = 1;
    }
    if (ucLED & LED3) {
        TSB_PG_DATA_PG2 = 1;
    }
    if (ucLED & LED4) {
        TSB_PG_DATA_PG3 = 1;
    }
    if (ucLED & LED5) {
        TSB_PJ_DATA_PJ0 = 1;
    }
    if (ucLED & LED6) {
        TSB_PJ_DATA_PJ1 = 1;
    }
    if (ucLED & LED7) {
        TSB_PJ_DATA_PJ2 = 1;
    }
    if (ucLED & LED8) {
        TSB_PJ_DATA_PJ3 = 1;
    }
}

/*******************************************************************************
* Function Name  : LED_Off
* Description    : Turn off LED
* Input          : ucLED
* Return         : None.
*******************************************************************************/
void LED_Off(uint8_t ucLED)
{
    if (ucLED & LED1) {
        TSB_PG_DATA_PG0 = 0;
    }
    if (ucLED & LED2) {
        TSB_PG_DATA_PG1 = 0;
    }
    if (ucLED & LED3) {
        TSB_PG_DATA_PG2 = 0;
    }
    if (ucLED & LED4) {
        TSB_PG_DATA_PG3 = 0;
    }
    if (ucLED & LED5) {
        TSB_PJ_DATA_PJ0 = 0;
    }
    if (ucLED & LED6) {
        TSB_PJ_DATA_PJ1 = 0;
    }
    if (ucLED & LED7) {
        TSB_PJ_DATA_PJ2 = 0;
    }
    if (ucLED & LED8) {
        TSB_PJ_DATA_PJ3 = 0;
    }
}

/*******************************************************************************
* Function Name  : LED_Toggle
* Description    : Toggle LED
* Input          : None.
* Return         : None.
*******************************************************************************/
void LED_Toggle(uint8_t ucLED)
{
    uint8_t ucVal = 0U;
    if (ucLED & LED1) {
        ucVal = (uint8_t) TSB_PG_DATA_PG0;
        ucVal ^= 0x01;
        TSB_PG_DATA_PG0 = ucVal;
    }
    if (ucLED & LED2) {
        ucVal = (uint8_t) TSB_PG_DATA_PG1;
        ucVal ^= 0x01;
        TSB_PG_DATA_PG1 = ucVal;
    }
    if (ucLED & LED3) {
        ucVal = (uint8_t) TSB_PG_DATA_PG2;
        ucVal ^= 0x01;
        TSB_PG_DATA_PG2 = ucVal;
    }
    if (ucLED & LED4) {
        ucVal = (uint8_t) TSB_PG_DATA_PG3;
        ucVal ^= 0x01;
        TSB_PG_DATA_PG3 = ucVal;
    }
    if (ucLED & LED5) {
        ucVal = (uint8_t) TSB_PJ_DATA_PJ0;
        ucVal ^= 0x01;
        TSB_PJ_DATA_PJ0 = ucVal;
    }
    if (ucLED & LED6) {
        ucVal = (uint8_t) TSB_PJ_DATA_PJ1;
        ucVal ^= 0x01;
        TSB_PJ_DATA_PJ1 = ucVal;
    }
    if (ucLED & LED7) {
        ucVal = (uint8_t) TSB_PJ_DATA_PJ2;
        ucVal ^= 0x01;
        TSB_PJ_DATA_PJ2 = ucVal;
    }   
    if (ucLED & LED8) {
        ucVal = (uint8_t) TSB_PJ_DATA_PJ3;
        ucVal ^= 0x01;
        TSB_PJ_DATA_PJ3 = ucVal;
    }
}
