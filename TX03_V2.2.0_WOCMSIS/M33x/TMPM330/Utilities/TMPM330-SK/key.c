/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* File Name          : key.c
* Version            : 
* Date               : 
* Description        : 
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include "key.h"

/*******************************************************************************
* Function Name  : KEY_Configuration
* Description    : Config the GPIO to KEY 
* Input          : None 
* Return         : None.
*******************************************************************************/
void KEY_Configuration(void)
{
    TSB_PB->IE |= 0x00F0;
    TSB_PB->PUP |= 0x00F0;
}

/*******************************************************************************
* Function Name  : KEY_Get
* Description    : Get KEY Value
* Input          : ucKEY.
* Return         : KEY value.
*******************************************************************************/
uint8_t KEY_Get(uint8_t ucKEY)
{
    uint8_t ucVal = 0U;
    if (ucKEY & KEY1) {
        if (!TSB_PB_DATA_PB7) {
            ucVal |= 0x01;
        }
    }
    if (ucKEY & KEY2) {
        if (!TSB_PB_DATA_PB6) {
            ucVal |= 0x02;
        }
    }
    if (ucKEY & KEY3) {
        if (!TSB_PB_DATA_PB5) {
            ucVal |= 0x04;
        }
    }
    if (ucKEY & KEY4) {
        if (!TSB_PB_DATA_PB4) {
            ucVal |= 0x08;
        }
    }
    return (ucVal);
}
