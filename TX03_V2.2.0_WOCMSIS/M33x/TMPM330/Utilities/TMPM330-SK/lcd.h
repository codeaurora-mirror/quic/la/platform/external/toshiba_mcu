/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* File Name          : led.h
* Version            : 
* Date               : 
* Description        : 
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/
#ifndef __LCD_H
#define __LCD_H

#include "TMPM330.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define ON  1
#define OFF 0

#define LCD_COMMAND    0
#define LCD_CHARACTER  1

#define   CLR_DISP      0x00000001
#define   DISP          0x00000008
#define   DISP_ON       0x0000000C
#define   DISP_CUR      0x0000000E
#define   DISP_BINK_CUR 0x0000000F
#define   DISP_OFF      DISP
#define   CUR_HOME      0x00000002
#define   SCUR_OFF      DISP_ON
#define	  CUR_UNDER     DISP_CUR
#define   CUR_BLINK     DISP_BINK_CUR
#define   CUR_LEFT      0x00000010
#define   CUR_RIGHT     0x00000014
#define   FIRST_LINE    0x00000080
#define   SECOND_LINE   0x000000C0

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* external variables --------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void E_Pulse(void);
void Set_Data(unsigned char data);
void Send_To_LCD(unsigned char data, unsigned char type);
void Send_LCD_Text(char *str);
void LCD_IO(void);
void LCD_Configuration(void);
void LCD_Light(unsigned char BLStatus);
void Delay(int time);

#endif                          /* LCD_H */
