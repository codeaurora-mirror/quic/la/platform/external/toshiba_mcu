/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* @file    led.h
* @brief   LED driver for the TOSHIBA 'TMPM330' Device Series
* @version V0.200
* @date    2010/06/15
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/
#ifndef __M330_LED_H
#define __M330_LED_H

#include "tmpm330_gpio.h"



/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

#define LEDALL_OFF    0x00
#define LEDALL_ON     0xff

#define LED1     0x01
#define LED2     0x02
#define LED3     0x04
#define LED4     0x08
#define LED5     0x10
#define LED6     0x20
#define LED7     0x40
#define LED8     0x80

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* external variables --------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void LED_Display(uint8_t ucLED);
void LED_Init(void);
void LED_On(uint8_t ucLED);
void LED_Off(uint8_t ucLED);

#endif                          /* __M330_LED_H */
