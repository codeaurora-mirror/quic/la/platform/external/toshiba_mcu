/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* @file    key.h
* @brief   Key driver for the TOSHIBA 'TMPM330' Device Series
* @version V0.200
* @date    2010/06/15
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/
#ifndef __M330_KEY_H
#define __M330_KEY_H

#include "tmpm330_gpio.h"


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

#define KEY1     0x01
#define KEY2     0x02
#define KEY3     0x04
#define KEY4     0x08

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* external variables --------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void KEY_Init(void);
uint8_t KEY_Get(uint8_t ucKEY);

#endif                          /* __M330_KEY_H */
