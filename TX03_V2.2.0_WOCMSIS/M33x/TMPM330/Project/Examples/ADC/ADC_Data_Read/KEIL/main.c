/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* @file    main.c
* @brief   The demo functions of ADC for the
*           TOSHIBA 'TMPM330' Device Series 
* @version V1.200
* @date    2010/06/17
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "main.h"

char tADC_Display[16];
uint32_t gADC_state = 0U;
ADC_ResultTypeDef gADC_Result;

int main(void)
{
    LCD_Configuration();
    LCD_Light(ON);

    ADC_SWReset();
    ADC_SetInputChannel(ADC_AN_0);
    ADC_SetRepeatMode(ENABLE);
    ADC_SetVref(ENABLE);
    ADC_Start();

    for (;;) {
        gADC_state = ADC_GetConvertState();
        if (DONE == gADC_state) {
            gADC_state = BUSY;
            gADC_Result = ADC_GetConvertResult(ADC_REG_08);
            ADC_Display(gADC_Result.ADCResultValue);
        }
    }
}

void ADC_Display(uint32_t e_Adc_Value)
{
    uint32_t e_volumn;
    uint32_t e_ADC_i;

    e_volumn = (e_Adc_Value * 100U) / 0x03ffU;  /* Max ADC is 0x03ff      */

    tADC_Display[0] = 'A';      /* Disaply: "ADC Value:   "     */
    tADC_Display[1] = 'D';
    tADC_Display[2] = 'C';
    tADC_Display[3] = ' ';
    tADC_Display[4] = 'V';
    tADC_Display[5] = 'a';
    tADC_Display[6] = 'l';
    tADC_Display[7] = 'u';
    tADC_Display[8] = 'e';
    tADC_Display[9] = ':';

    /*  Display the value                           */
    if (e_volumn >= 100U) {
        tADC_Display[10] = (char) ((e_volumn / 100U) + 0x30U);
        tADC_Display[11] = (char) (((e_volumn / 10U) % 10U) + 0x30U);
        tADC_Display[12] = (char) ((e_volumn % 10U) + 0x30U);
    } else {
        tADC_Display[10] = ' ';
        if (e_volumn / 10U) {
            tADC_Display[11] = (char) ((e_volumn / 10U) + 0x30U);
        } else {
            tADC_Display[11] = ' ';
        }
        tADC_Display[12] = (char) ((e_volumn % 10U) + 0x30U);
    }
    tADC_Display[13] = 0U;
    Send_To_LCD(FIRST_LINE, LCD_COMMAND);
    Send_LCD_Text(tADC_Display);

    /*  Display the value volumn percent, the 100% is 16 x '*'      */
    e_volumn = e_volumn * 16U;
    e_volumn = e_volumn / 100U;
    for (e_ADC_i = 0U; e_ADC_i < 16U; e_ADC_i++) {
        tADC_Display[e_ADC_i] = 0U;
    }

    for (e_ADC_i = 0U; e_ADC_i < e_volumn; e_ADC_i++) {
        tADC_Display[e_ADC_i] = '*';    /* Display "*" for volumn percent     */
    }
    e_ADC_i++;
    tADC_Display[e_ADC_i] = 0U; /* String end               */
    Send_To_LCD(SECOND_LINE, LCD_COMMAND);
    Send_LCD_Text(tADC_Display);
}

#ifdef DEBUG
void assert_failed(char *file, int32_t line)
{
    while (1) {
        __NOP();
    }
}
#endif
