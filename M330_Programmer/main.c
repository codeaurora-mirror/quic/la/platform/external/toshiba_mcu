/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* File Name          : main.c
* Version            : 
* Date               : 
* Description        : 
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "stdafx.h"
#include <windows.h>
#include "winbase.h"

#define M330_PAGE_SIZE (512)

/*
 * Main function reads the input binary file 512 bytes at a time (Page size)
 * and send it to the Cortex Programming routine over UART.
 */
int main(int argc, char *argv[])
{
	unsigned int i, page_no, no_bytes_read;
	int send_termination_cmd;
	HANDLE hSerial;
	FILE *Fp = NULL;
	COMMTIMEOUTS timeouts;
	DCB dcb;
	DWORD dwBytesWritten, dwBytesRead;
	BOOL fSuccess;

	char com_port[20];
	WCHAR com_port_w[20];
	unsigned char buffer[M330_PAGE_SIZE];
	unsigned char buffRead[10];

	if(argc < 3)
	{
		printf("\n");
		printf("-------------------- Usage --------------------\n");
		printf("m330_programmer <Comport number> <input file> \n");
		printf(" -> Comport number: i.e. COM1 \n");
		printf(" -> Input file    : binary file to be written \n");
		printf("-----------------------------------------------\n");

		getchar();
		return 1;
	}

	memset(com_port,0,sizeof(com_port));
	memset(com_port_w,0,sizeof(com_port_w));
	sprintf_s(com_port, sizeof(com_port), "\\\\.\\%s", argv[1]);

	for(i=0; i<sizeof(com_port); i++)
		com_port_w[i] = com_port[i];

	hSerial = CreateFile((LPCWSTR)com_port_w, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if ( hSerial == INVALID_HANDLE_VALUE)
	{
		printf("COM port opening error\n");
		getchar();
		CloseHandle(hSerial);
		return 1;
	}

	SecureZeroMemory(&dcb, sizeof(dcb));
	dcb.DCBlength = sizeof (dcb);

	fSuccess = GetCommState( hSerial, &dcb);
	if (!fSuccess) 
	{
	    printf ("GetCommState failed with error %d.\n", GetLastError());
  	    getchar();
		CloseHandle(hSerial);
	    return 1;
	}

	dcb.BaudRate = CBR_57600;
	dcb.ByteSize = 8;
	dcb.StopBits = ONESTOPBIT;
	dcb.Parity = NOPARITY;
	dcb.fBinary = TRUE;
	dcb.fDtrControl = DTR_CONTROL_DISABLE;
	dcb.fRtsControl = RTS_CONTROL_DISABLE;
	dcb.fOutxCtsFlow = FALSE;
	dcb.fOutxDsrFlow = FALSE;
	dcb.fDsrSensitivity= FALSE;
	dcb.fAbortOnError = TRUE;
	if (!SetCommState(hSerial, &dcb))
	{
		printf(" error setting serial port state \n");
		getchar();
		CloseHandle(hSerial);
	    return 1;
	}

	if (!GetCommState(hSerial, &dcb))
	{
		printf("error getting serial port state \n");
		getchar();
		CloseHandle(hSerial);
	    return 1;
	}

	GetCommTimeouts(hSerial,&timeouts);

	timeouts.ReadIntervalTimeout = 50;
	timeouts.ReadTotalTimeoutConstant = 50;
	timeouts.ReadTotalTimeoutMultiplier = 10;
	timeouts.WriteTotalTimeoutConstant = 50;
	timeouts.WriteTotalTimeoutMultiplier= 10;

	if(!SetCommTimeouts(hSerial, &timeouts))
	{
		printf("error setting port state \n");
		getchar();
		CloseHandle(hSerial);
	    return 1;
	}

	memset(buffRead,0,sizeof(buffRead));

	page_no = 0;
	send_termination_cmd = 0;

	printf("Waiting for Request from Cortex \n\n");

	fopen_s(&Fp, argv[2], "rb");
	if (Fp != NULL)
	{
		while(1)
		{
			memset(buffRead,0,sizeof(buffRead));
			if (!ReadFile(hSerial, buffRead, 1, &dwBytesRead, NULL))
			{
				printf("error reading from input buffer \n");
				getchar();
				fclose(Fp);
				CloseHandle(hSerial);
				return 1;
			}

			if(buffRead[0] == 'S')
			{
				memset(buffer,0,sizeof(buffer));
				if(send_termination_cmd == 0)
				{
					no_bytes_read = (unsigned int)fread(buffer, 1, M330_PAGE_SIZE, Fp);

					if(feof(Fp) != 0)
						send_termination_cmd = 1;
				
					printf("Bytes read from file= %d\t\t", no_bytes_read);
					printf("Sending Page = %d \n", page_no++);

//					for(i=0; i<M330_PAGE_SIZE; i++)
//						printf("%02x ", buffer[i]);

					if (!WriteFile(hSerial, buffer, M330_PAGE_SIZE, &dwBytesWritten, NULL))
					{
						printf("error writing to output buffer \n");
						getchar();
						fclose(Fp);
						CloseHandle(hSerial);
						return 1;
					}
				}
				else
				{
					printf("\nSending Terminate Command");
					strcpy_s((char*)buffer, sizeof("TERMINATE"), "TERMINATE");

					if (!WriteFile(hSerial, buffer, M330_PAGE_SIZE, &dwBytesWritten, NULL))
					{
						printf("error writing to output buffer \n");
						getchar();
						fclose(Fp);
						CloseHandle(hSerial);
						return 1;
					}

					break;
				}					
			}
		}

		fclose(Fp);
	}
	else
	{
		printf("File opening error\n");
		getchar();
		CloseHandle(hSerial);
		return 1;
	}

	CloseHandle(hSerial);

	return 0;
}
